import User from '../models/User';
import Channel from '../models/Channel';

const user = async (root, { id: _id }) =>
  User.findOne({ _id });
const allUsers = async () =>
  User.find({});
const me = (root, args, { user: { id } }) =>
  User.findById(id);
const channel = async (root, { id: _id }) =>
  Channel.findOne({ _id });
const allChannels = async () =>
  Channel.find({});

export { user, allUsers, me, channel, allChannels };
