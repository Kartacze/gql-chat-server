import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

import User from '../models/User';

const me = (root, args, context) =>
  context.user;

const login = async (root, { email, password }, { key }) => {
  const user = await User.findOne({ email });
  if (!user) {
    throw new Error('Email not found');
  }

  const validPassword = await bcrypt.compare(password, user.password);
  if (!validPassword) {
    throw new Error('Password is incorrect');
  }

  user.jwt = jwt.sign({ id: user.id }, key);

  return user.jwt;
};

const signup = async (root, { email, password, username }, { key }) => {
  const existingUser = await User.findOne({ email });

  if (existingUser) {
    throw new Error('Email already used');
  }

  const hash = await bcrypt.hash(password, 10);
  await User.insertMany({
    email,
    password: hash,
    username,
    created: Date(),
  });
  const user = await User.findOne({ email });

  user.jwt = jwt.sign({ id: user.id }, key);

  return user.jwt;
};

export { me, login, signup };

