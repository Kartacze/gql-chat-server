import mongoose from 'mongoose';
import { GraphQLScalarType, Kind } from 'graphql';

import User from '../models/User';
import Message from '../models/Message';
import Channel from '../models/Channel';


import { login, signup } from './authResolver';
import { user, allUsers, me, channel, allChannels } from './queryResolvers';

const { ObjectId } = mongoose.Types;

const DateQ = new GraphQLScalarType({
  name: 'DateQ',
  description: 'This is Date description',
  serialize(value) { // to client
    return value;
  },
  parseValue(value) { // from client
    return new Date(value);
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return parseInt(ast.value, 10);
    }
    return null;
  },
});

const resolvers = {
  Query: {
    user,
    channel,
    allUsers,
    allChannels,
    me,
  },
  User: {
    channels: async ({ channels }) => Channel.find({ _id: { $in: channels } }),
  },
  Channel: {
    admins: async ({ admins }) => User.find({ _id: { $in: admins } }),
    messages: async ({ messages }) => Message.find({ _id: { $in: messages } }),
    users: async ({ users }) => User.find({ _id: { $in: users } }),
  },
  Message: {
    user: async ({ user }) => User.findById(user),
  },
  Mutation: {
    createChannel: async (root, { adminId, ...rest }) => {
      const id = ObjectId(adminId);
      const resp = await Channel.insertMany({
        admins: [id],
        users: [id],
        _id: ObjectId(),
        ...rest,
      });
      User.findByIdAndUpdate(adminId, { $push: { channels: resp[0]._id } });
      return resp[0];
    },
    deleteChannel: async (root, { id }) => Channel.findByIdAndRemove(id),
    addMessage: async (root, args) => {
      const msg = await Message.insertMany({
        ...args,
        date: Date(),
        _id: ObjectId(),
      });
      await Channel.findByIdAndUpdate(args.channelId, {
        $push: { messages: msg[0].id },
      });
      return msg[0];
    },
    deleteMessage: async (_, { id }) => Message.findByIdAndRemove(id),
    login,
    signup,
  },
  DateQ,
};

export default resolvers;
