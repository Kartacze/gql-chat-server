import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
// const { ObjectId } = mongoose.Types;

const config = {
  connection: null,
};

const connect = async uri =>
  new Promise((resolve, reject) => {
    if (config.connection) {
      return resolve();
    }
    mongoose.connect(uri, {
      useMongoClient: true,
    });

    config.connection = mongoose.connection;

    config.connection
      .once('open', resolve)
      .on('error', (e) => {
        if (e.message.code === 'ETIMEDOUT') {
          console.log(e);
          mongoose.connect(uri);
        }
        console.log(e);
        reject(e);
      });
  });

const clearDatabase = async () =>
  new Promise((resolve) => {
    let cont = 0;
    const max = Object.keys(mongoose.connection.collections).length;
    Object.keys(mongoose.connection.collections).forEach((e) => {
      mongoose.connection.collections[e].remove(() => {
        cont += 1;
        if (cont >= max) {
          resolve();
        }
      });
    });
  });

export { connect, clearDatabase };

