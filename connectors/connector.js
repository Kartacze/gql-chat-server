import Mongoose from 'mongoose';

const schema = Mongoose.Schema;
Mongoose.Promise = global.Promise;

const setVirtual = (schema) => {
  schema.virtual('id').get(function () {
    return this._id.toString();
  });
  schema.set('toJSON', {
    virtuals: true
  })
}

const mongo = Mongoose.connect('mongodb://localhost:27017/graphqlChat', {
  useMongoClient: true
});

const UserSchema = Mongoose.Schema({
  _id: schema.Types.ObjectId,
  username: String,
  posts: [{
    type: schema.Types.ObjectId
  }],
  email: String,
  password: String,
  channels: [{
    type: schema.Types.ObjectId,
    ref: 'channel'
  }],
  friends: [{
    type: schema.Types.ObjectId
  }],
});

const ChannelSchema = Mongoose.Schema({
  _id: schema.Types.ObjectId,
  name: String,
  private: Boolean,
  admins: [{
    type: schema.Types.ObjectId
  }],
  messages: [{
    type: schema.Types.ObjectId
  }],
  users: [{
    type: schema.Types.ObjectId
  }],
});

const MessageSchema = Mongoose.Schema({
  _id: schema.Types.ObjectId,
  userId: schema.Types.ObjectId,
  text: String,
  createdAt: Date,
  channelId: schema.Types.ObjectId
});

setVirtual(UserSchema);
setVirtual(ChannelSchema);
setVirtual(MessageSchema);


const User = Mongoose.model('user', UserSchema);
const Channel = Mongoose.model('channel', ChannelSchema);
const Message = Mongoose.model('message', MessageSchema);

export {
  User,
  Message, 
  Channel,
};