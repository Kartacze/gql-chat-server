import { makeExecutableSchema } from 'graphql-tools';
import resolvers from '../resolvers/resolvers';


const typeDefs = `
  scalar DateQ

  type Query {
    user(id: String): User
    allUsers : [User]
    channel(id: String): Channel 
    allChannels : [Channel]
    me: User
  }

  type User {
    id: String!
    username: String!
    created: DateQ!
    email: String!
    channels: [Channel]
    lastLogin: DateQ
    lastIP: String
  }

  type Channel {
    id: String
    name: String!
    admins: [User!]!
    messages: [Message]
    private: Boolean!
    users: [User]
    created: DateQ
  }

  type Message {
    id: String!
    user: User!
    text: String!
    createdAt: DateQ
    channelId: String!
  }

  type Mutation {
    createChannel(name: String!, adminId: String!, private: Boolean!): Channel
    deleteChannel(id: String!): Boolean
    addMessage(user: String!, text: String!, channelId: String!): Message
    deleteMessage(id: String!): Boolean
    login(email: String!, password: String!): String
    signup(email: String!, password: String!, username: String!): String 
  }
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

export default schema;
