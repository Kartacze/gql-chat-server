import Mongoose from 'mongoose';
import { setVirtual } from './helpers';

const schema = Mongoose.Schema;

const MessageSchema = Mongoose.Schema({
  _id: schema.Types.ObjectId,
  user: schema.Types.ObjectId,
  text: String,
  createdAt: Date,
  channelId: schema.Types.ObjectId,
});

setVirtual(MessageSchema);
const Message = Mongoose.model('message', MessageSchema);

export default Message;
