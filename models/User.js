import Mongoose from 'mongoose';
import { setVirtual } from './helpers';

const schema = Mongoose.Schema;

const UserSchema = Mongoose.Schema({
  _id: schema.Types.ObjectId,
  username: String,
  email: String,
  password: String,
  channels: [
    {
      type: schema.Types.ObjectId,
      ref: 'channel',
    },
  ],
  created: {
    type: Date,
    default: Date.now,
  },
  lastLogin: Date,
  lastIP: String,
});

setVirtual(UserSchema);
const User = Mongoose.model('user', UserSchema);

export default User;
