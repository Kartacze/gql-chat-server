const setVirtual = (schema) => {
  schema.virtual('id').get(function () {
    return this._id.toString();
  });
  schema.set('toJSON', {
    virtuals: true,
  });
};

export { setVirtual };
