import Mongoose from 'mongoose';
import { setVirtual } from './helpers';

const schema = Mongoose.Schema;

const ChannelSchema = Mongoose.Schema({
  _id: schema.Types.ObjectId,
  name: String,
  private: Boolean,
  created: Date,
  admins: [
    {
      type: schema.Types.ObjectId,
    },
  ],
  messages: [
    {
      type: schema.Types.ObjectId,
    },
  ],
  users: [
    {
      type: schema.Types.ObjectId,
    },
  ],
});

setVirtual(ChannelSchema);
const Channel = Mongoose.model('channel', ChannelSchema);

export default Channel;
