import { graphql } from 'graphql';
import schema from '../schema/schema';
import User from '../models/User';
import Channel from '../models/Channel';
import Message from '../models/Message';
import setupTest from './helper';


describe('testing Channel queries and mutations', () => {
  const user1 = {
    email: 'test1@some.com',
    password: 'dupa',
    username: 'test1',
  };

  const user2 = {
    email: 'test2@some.com',
    password: 'dupa',
    username: 'test2',
  };

  let chanId;
  let channel;
  let msgId;

  beforeAll(async () => {
    await setupTest();
    await User.insertMany([user1, user2]);
    await User.findOne({ email: user1.email }, (err, found) => {
      if (err) {
        throw Error('database error', err);
      }
      user1.id = found._id.toString();
    });
    await User.findOne({ email: user2.email }, (err, found) => {
      if (err) {
        throw Error('database error', err);
      }
      user2.id = found._id.toString();
    });
    channel = {
      name: 'channel1',
      adminId: user1.id,
      private: false,
    };
    await Channel.insertMany(channel);
  });

  it('should query allChannels', async () => {
    // language=GraphQL
    const query = `
      query {
        allChannels {
          id
        }
      }
    `;

    const rootValue = {};
    const result = await graphql(schema, query, rootValue);
    const { data: { allChannels: chans } } = result;

    expect(result.errors).toBeUndefined();
    expect(chans).toBeDefined();

    chanId = chans[0].id;
  });

  it('should add properly a message', async () => {
    // language=GraphQL
    const query = `
      mutation {
        addMessage(user: "${user1.id}", text: "some message", channelId: "${chanId}") {
          id
          user {
            id
          }
          channelId
          text
        }
      }
    `;

    const rootValue = {};
    const result = await graphql(schema, query, rootValue);
    const { data: { addMessage: msg } } = result;
    expect(result.errors).toBeUndefined();
    expect(msg).toBeDefined();
    expect(msg.user.id).toBe(user1.id);
    expect(msg.channelId).toBe(chanId);
    expect(msg.text).toBe("some message");

    // Channel should also contain this message
    const gchan = await Channel.findById(chanId);

    expect(gchan).toBeDefined();
    expect(gchan.messages.length).toBe(1);
    expect(gchan.messages[0]).not.toBe(null);
    msgId = msg.id;
  });

  it('should delete the message', async () => {
    // language=GraphQL
    const query = `
      mutation {
        deleteMessage(id: "${msgId}") 
      }
    `;
    const rootValue = {};
    const result = await graphql(schema, query, rootValue);
    expect(result.errors).toBeUndefined();

    const dltMsg = await Message.findById(msgId);
    expect(dltMsg).toBe(null);
  });
});
