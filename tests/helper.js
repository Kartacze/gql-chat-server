import { connect, clearDatabase } from '../mongo/connector';

const setupTest = async () => {
  await connect('mongodb://localhost:27017/graphqlChatTest');
  await clearDatabase();
};

export default setupTest;
