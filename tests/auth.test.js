import { graphql } from 'graphql';
import schema from '../schema/schema';
import User from '../models/User';
import setupTest from './helper';

import { secret } from '../config';


describe('A user', () => {
  beforeAll(async () => await setupTest());

  let token;
  const user1 = {
    email: 'test@a.com',
    password: 'dupa',
    username: 'test',
  };

  it('the user list should be empty and request successfull', async () => {
    // language=GraphQL
    const query = `
      query {
        allUsers {
          username
        }
      }
    `;

    const rootValue = {};
    const result = await graphql(schema, query, rootValue);
    const { data } = result;

    expect(result.errors).toBeUndefined();
    expect(data.allUsers.length).toBe(0);
  });

  it('signup should return a token', async () => {
    // language=GraphQL
    const query = `
      mutation {
        signup(email: "${user1.email}", password: "${user1.password}", username: "${user1.username}")
      }
    `;
    const rootValue = {};
    const context = { key: secret };
    const result = await graphql(schema, query, rootValue, context);
    const { data } = result;

    expect(result.errors).toBeUndefined();
    expect(data.signup).toBeDefined();
    
    token = data.signup;
  });


  // this should be tested somewhere else
  it('check if user id added properly', async () => {
    // language=GraphQL
    const query = `
      query {
        allUsers {
          username
          id
          created
        }
      }
    `;
    const rootValue = {};
    const context = { key: secret };
    const result = await graphql(schema, query, rootValue, context);
    const { data } = result;

    expect(data.allUsers.length).toBe(1);
    expect(data.allUsers[0].username).toBe(user1.username);

    user1.id = data.allUsers[0].id;
  });

  it('check if will return properl user', async () => {
    // language=GraphQL
    const query = `
      query {
        me {
          id
          email
          username
          channels {
            id
          }
          created
        }
      }
    `;
    const rootValue = {};
    const context = { key: secret, user: { id: user1.id } };
    const result = await graphql(schema, query, rootValue, context);
    expect(result.errors).toBeUndefined();
    const { data } = result;
    expect(data.me).toBeDefined();
    expect(data.me.id).toBeDefined();
    expect(data.me.email).toBeDefined();
    expect(data.me.username).toBeDefined();
    expect(data.me.id).toBe(user1.id);
    expect(data.me.created).toBeDefined();
  });
});
