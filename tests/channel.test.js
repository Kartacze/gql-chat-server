import { graphql } from 'graphql';
import schema from '../schema/schema';
import User from '../models/User';
import setupTest from './helper';

import { secret } from '../config';
import { Error } from 'mongoose';


describe('testing Channel queries and mutations', () => {
  const user = {
    email: 'testChannel@a.com',
    password: 'dupa',
    username: 'test',
  };

  let userId;
  let channel;
  let channelId;

  beforeAll(async () => {
    await setupTest();
    await User.insertMany(user);
    await User.findOne({ email: user.email }, (err, found) => {
      if (err) {
        console.log('error ', err);
        throw Error('database error');
      }
      userId = found._id.toString();
    });
    channel = {
      name: 'channel1',
      adminId: userId,
      private: false,
    };
  });

  it('should add properly channel', async () => {
    // language=GraphQL
    const query = `
      mutation {
        createChannel(name: "${channel.name}", 
          adminId: "${channel.adminId}",
          private: ${channel.private}) {
            id 
            name
            private
            admins {
              id 
            }
            users {
              id
            }
          }
      }
    `;

    const rootValue = {};
    const result = await graphql(schema, query, rootValue);
    const { data: { createChannel: chan } } = result;

    expect(result.errors).toBeUndefined();
    expect(chan).toBeDefined();
    expect(chan.id).toBeDefined();
    expect(chan.name).toBeDefined();
    expect(chan.private).toBeDefined();
    expect(chan.admins).toBeDefined();
    expect(chan.users).toBeDefined();
    expect(chan.users[0].id).toBe(userId);
    expect(chan.admins[0].id).toBe(userId);

    channelId = chan.id;
  });


  it('should add properly channel', async () => {
    // language=GraphQL
    const query = `
      query {
        channel(id: "${channelId}") {
          id
          name
          private
          admins {
            id
          }
          users {
            id
          }
        }
      }
    `;

    const rootValue = {};
    const result = await graphql(schema, query, rootValue);
    const { data: { channel: chan } } = result;

    expect(result.errors).toBeUndefined();
    expect(chan).toBeDefined();
    expect(chan.id).toBeDefined();
    expect(chan.name).toBeDefined();
    expect(chan.private).toBeDefined();
    expect(chan.admins).toBeDefined();
    expect(chan.users).toBeDefined();
    expect(chan.users[0].id).toBe(userId);
    expect(chan.admins[0].id).toBe(userId);
  });
});
