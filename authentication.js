import jwt from 'express-jwt';
import jwtDecode from 'jwt-decode';


const jwtMiddleware = jwt({ secret: 'some-strong-secret-key' });

const getUserFromJwt = (req, res, next) => {
  const authHeader = req.headers.authorization;
  req.user = jwtDecode(authHeader);
  next();
};

const auth = (app) => {
  app.use(jwtMiddleware);
  app.use(getUserFromJwt);
};

export default auth;
