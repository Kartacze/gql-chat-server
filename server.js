import express from 'express';
import {
  graphqlExpress,
  graphiqlExpress,
} from 'apollo-server-express';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';
import cors from 'cors';
import schema from './schema/schema';
import { connect } from './mongo/connector';
import { secret, mongoUrl } from './config';

const GRAPHQL_PORT = 3000;

const graphQLServer = express();

connect(mongoUrl);

const getUserFromJwt = (req, res, next) => {
  const authHeader = req.headers.authorization;
  req.user = undefined;
  if (authHeader) {
    jwt.verify(authHeader, secret, (err, decoded) => {
      if (err) {
        req.user = undefined;
        next(err);
      }
      req.user.id = decoded.id;
    });
  }
  next();
};

graphQLServer.use(getUserFromJwt);
graphQLServer.use(cors());

graphQLServer.use('/graphql', bodyParser.json(), graphqlExpress(({ user }) => ({
  schema,
  context: { key: secret, user },
  graphiql: true,
})));

graphQLServer.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
}));

graphQLServer.listen(GRAPHQL_PORT, () =>
  console.log(`GraphiQL is now running on http://localhost:${GRAPHQL_PORT}/graphiql`));
